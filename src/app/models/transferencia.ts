export class DatosTransf {
  constructor(_id = '', banco = '') {
    this._id = _id;
    this.banco = banco;
  }
  _id: string;
  banco: string;
}
