import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { EstadoTransferenciasComponent } from './components/estado-transferencias/estado-transferencias.component';
import { AgregarTransferenciaComponent } from './components/agregar-transferencia/agregar-transferencia.component';
import { NumberDirective } from './directives/number-only.directive';

@NgModule({
  declarations: [AppComponent, EstadoTransferenciasComponent, AgregarTransferenciaComponent, NumberDirective],
  imports: [BrowserModule, HttpClientModule, FormsModule, ReactiveFormsModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
