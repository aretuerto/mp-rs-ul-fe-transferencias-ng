import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgregarTransferenciaComponent } from './agregar-transferencia.component';

describe('AgregarTransferenciaComponent', () => {
  let component: AgregarTransferenciaComponent;
  let fixture: ComponentFixture<AgregarTransferenciaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AgregarTransferenciaComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgregarTransferenciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
