import { Component, ElementRef, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ConnectService } from './../../services/connect.service';
import * as _ from 'lodash';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-agregar-transferencia',
  templateUrl: './agregar-transferencia.component.html',
  styleUrls: ['./agregar-transferencia.component.scss']
})
export class AgregarTransferenciaComponent implements OnInit {

  url: string;
  transfForm: FormGroup;
  successTransfer: boolean;
  exampleBank: string;
  submitted: boolean;
  baseFiles: Array<any>;
  ImageBaseData: string | ArrayBuffer | null;
  bancos: Array<any> = [];

  transferencia: Array<any> = [];
  tBanco: string;
  tFecha: string;
  tIdCtaBca: number;
  tEstado: number;
  tId: number;
  tImporOpe: number;
  tMoneda: string;
  tNumOpe: string;

  nameImg: string;
  typeImg: string;
  imageError: string;
  isImageSaved: boolean;
  cardImageBase64: string;
  imagenBase64: string;
  bancoSelect: Array<any>;

  time = new Date();
  fechaC: string;
  hora: string;
  nombre: string;
  extension: string;

  constructor(private _formBuilder: FormBuilder, private _transfService: ConnectService) {}

  ngOnInit() {
    console.log("se va a editar? "+this._transfService.editar)
    this.hora = formatDate(this.time, 'hh:mm:ss', 'en-US', '-0500');
    this.successTransfer = false;
    this.exampleBank = '';
    this.url = 'http://8603-161-132-234-177.ngrok.io';
    this.bancoSelect = [{}];

    //formulario
    this.transfForm = this._formBuilder.group({
      //campos desde servicio (hidden)
      codigoCompania: [''],
      numPoliza: [''],
      codigoRamo: [''],
      numSpto: [''],
      nombreObjetivo: [''],
      importeInversion: [''],
      moneda: [''],
      extensionArchivo: [''],
      nombreArchivo: [''],
      numeroDocumento: [''],
      tipoDocumento: [''],
      //campos desde el form
      idTransferencia: [''],
      bancoLista: ['', Validators.required],
      fecha: ['', Validators.required],
      idCuentaBancaria: ['', Validators.required],
      numeroOperacion: ['', Validators.required],
      importeOperacion: ['', Validators.required],
      imagenFile: ['', Validators.required],
      imagenOperacion: ['']
    });

    //obtener listado de bancos
    this.getBancos();

    //consultas si hay que editar transferencia
    //console.log("id: "+this._transfService.idEdicion);
    if(this._transfService.editar){
      console.log('entra para editar');
      this._transfService.editarTransferencia(this._transfService.idEdicion).subscribe((res: any) =>{
        this.transferencia = res;
        this.tBanco = res.Data[0].dscBanco;
        this.tFecha = res.Data[0].fecha;
        this.tId = res.Data[0].id;
        this.tEstado = res.Data[0].estado;
        this.tIdCtaBca = res.Data[0].idCuentaBancaria;
        this.tImporOpe = res.Data[0].importeOperacion;
        this.tMoneda = res.Data[0].moneda;
        this.tNumOpe = res.Data[0].numeroOperacion;
        console.log(this.transferencia);
        
      })
    }
  }

  //llama servicio para obtener bancos
  getBancos() {
    this._transfService.getBancos().subscribe((res: any) => {
      this.bancos = res;
      //console.log(this.bancos);
      //let ctas = this.bancos.find((data:any) => data.Data.banco === 'CONTINENTAL')
      // let ctas: Array<any> = this.bancos
      // ctas = ctas.filter(s => s.Data.banco === 'CONTINENTAL')
      // console.log(ctas);
    });
  }

  //selecciona un banco
  selectBanco(b: any) {
    console.log(b);
    this.bancoSelect = b;
  }

  //cambio de imagen example
  changeExample(e: any) {
    if (e == 'CREDITO') {
      this.exampleBank = './../assets/img/bcp_select.png';
    } else if (e == 'CONTINENTAL') {
      this.exampleBank = './../assets/img/docu.png';
    } else if (e == 'INTERBANK') {
      this.exampleBank = './../assets/img/interbank_select.png';
    } else if (e == 'SCOTIABANK') {
      this.exampleBank = './../assets/img/scotiabank_select.png';
    } else if ( e == 'CITIBANK'){
      this.exampleBank = './../assets/img/citibank_select.png';
    } else if (e == 'FINANCIERO'){
      this.exampleBank = './../assets/img/financiero_select.png';
    } else if (e == 'BANBIF') {
      this.exampleBank = './../assets/img/banbif_select.png';
    }

  }

  //guarda a base68 la imagen
  fileChangeEvent(fileInput: any) {
    this.imageError = null;
    if (fileInput.target.files && fileInput.target.files[0]) {
      // tamaño de la imagen
      const max_size = 5000000;
      const allowed_types = ['image/png', 'image/jpeg', 'image/jpg', 'application/pdf', 'application/vnd.ms-excel', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'];
      const max_height = 15200;
      const max_width = 25600;

      //guarda el nombre y tipo del archivo
      console.log(fileInput.target.files[0]);
      this.nameImg = fileInput.target.files[0].name;
      this.typeImg = fileInput.target.files[0].type;

      if (fileInput.target.files[0].size > max_size) {
        this.imageError = 'Maximum size allowed is ' + max_size / 1000 + 'Mb';
        return false;
      }

      if (!_.includes(allowed_types, fileInput.target.files[0].type)) {
        this.imageError = 'Only Images are allowed ( JPG | PNG )';
        return false;
      }
      const reader = new FileReader();
      reader.onload = (e: any) => {
        const image = new Image();
        image.src = e.target.result;
        image.onload = rs => {
          const img_height = rs.currentTarget['height'];
          const img_width = rs.currentTarget['width'];
          //console.log(img_height, img_width);
          if (img_height > max_height && img_width > max_width) {
            this.imageError = 'Maximum dimentions allowed ' + max_height + '*' + max_width + 'px';
            return false;
          } else {
            const imgBase64Path = e.target.result;
            const hola = imgBase64Path.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);
            this.imagenBase64 = hola[2];
            this.cardImageBase64 = imgBase64Path;
            //console.log(this.cardImageBase64);
            this.isImageSaved = true;
            // this.previewImagePath = imgBase64Path;
          }
        };
      };
      reader.readAsDataURL(fileInput.target.files[0]);
    }
  }

  //limpia el input type file
  removeImage() {
    this.cardImageBase64 = null;
    this.isImageSaved = false;
    this.transfForm.controls['imagenOperacion'].setValue('');
    this.nameImg = null;
    this.nameImg = null;
  }

  get f() {
    return this.transfForm.controls;
  }

  //envio del formulario
  onSubmit() {
    this.submitted = true;
    if (this.transfForm.invalid) {
      console.log("form invalid: "+this.transfForm.controls);
      return;
    }
    console.log('entra submit: ');
    this.nombre = this.nameImg.split('.')[0];
    this.extension = this.nameImg.split('.')[1]
    this.fechaC = this.transfForm.controls['fecha'].value
    this.fechaC = this.fechaC.split('-').join('/')
    this.fechaC = this.fechaC.split('/')[2]+"/"+this.fechaC.split('/')[1]+"/"+this.fechaC.split('/')[0]
    this.transfForm.controls['fecha'].setValue(this.fechaC+ " " +this.hora);
    this.transfForm.controls['imagenOperacion'].setValue(this.imagenBase64);
    this.transfForm.controls['nombreArchivo'].setValue(this.nombre);
    this.transfForm.controls['tipoDocumento'].setValue(this._transfService.tipoDocumento);
    this.transfForm.controls['codigoCompania'].setValue(this._transfService.codigoCompania);
    this.transfForm.controls['numPoliza'].setValue(this._transfService.numPoliza);
    this.transfForm.controls['codigoRamo'].setValue(this._transfService.codRamo);
    this.transfForm.controls['numSpto'].setValue(this._transfService.numSpto);
    this.transfForm.controls['nombreObjetivo'].setValue(this._transfService.nombreObjetivo);
    this.transfForm.controls['importeInversion'].setValue(this._transfService.importeInversion);
    this.transfForm.controls['moneda'].setValue(this._transfService.tipoMoneda);
    this.transfForm.controls['numeroDocumento'].setValue(this._transfService.numeroDocumento);
    this.transfForm.controls['extensionArchivo'].setValue(this.extension);
    console.log(this.transfForm.value); 
    setTimeout(() => {
      this._transfService.postTransf(this.transfForm.value).subscribe(
        res => {
          console.log(res);
        },
        error => {
          console.log(error);
        }
      );
    }, 500);

    // setTimeout(() => {
    //   const httpOption : Object = {
    //     headers: new HttpHeaders({
    //       "Access-Control-Allow-Methods": "GET, POST, OPTIONS, PUT, DELETE",
    //       "token": "asdasd",
    //       "type": "text"
    //     }),
    //   }
    //   this._http.post(this.url+'/transferencia', this.transfForm.value, httpOption).subscribe(
    //     (res) => {
    //       console.log("respuesta: " +res);
    //       this.successTransfer = true;
    //     }, (error) => {
    //       console.log("respuesta error: " +error);
    //     }
    //   )
    // }, 900);
  }
}
