import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ConnectService } from '../../services/connect.service';

@Component({
  selector: 'app-estado-transferencias',
  templateUrl: './estado-transferencias.component.html',
  styleUrls: ['./estado-transferencias.component.scss'],
  providers: [ConnectService]
})
export class EstadoTransferenciasComponent implements OnInit {

  @Input() codCompania: number;
  @Input() numPoli: string;
  @Input() codiRamo: number;
  @Input() nmStpo: number;
  @Output() editaT = new EventEmitter<boolean>();
  @Output() estadoT = new EventEmitter<boolean>();
  @Output() idTransf = new EventEmitter<number>();
  @Input() metaInversion: any;

  inversionTope: number;
  inverActual: number;
  metaInver: boolean;
  resta: number;
  datos: any;
  rdy: boolean = false;
  modalDelete: boolean = false;
  sumaTotal: number = 0
  nIterativo: number;
  

  constructor(private _transfService: ConnectService) {}

  ngOnInit() {
    setTimeout(() => {
      this.getTransferencias(this.codCompania, this.numPoli, this.codiRamo, this.nmStpo);
    }, 900)
    
    this.inverActual = 12000;
    this.inversionTope = 12000;
    this.resta = this.inversionTope - this.inverActual;
    if (this.inversionTope > this.inverActual) {
      this.metaInver = true;
    } else {
      this.metaInver = false;
    }
    console.log(this.metaInversion);
  }
  getTransferencias(a: number, b: string, c: number, d: number) {
    this._transfService.listTransferencias(a,b,c,d).subscribe(res => {
      this.datos = res;
      console.log('¤ lista transferencias: ¬');
      console.log(this.datos);
      for( let valor of this.datos.Data){
        //this.nIterativo = valor.importeOperacion
        //this.sumaTotal = this.sumaTotal + this.nIterativo
        //console.log(this.sumaTotal);
        //console.log(Number.isNaN(valor.importeOperacion));
        //console.log(!Number.isNaN(this.sumaTotal));
        this.sumaTotal = this.sumaTotal + valor.importeOperacion
        //console.log("¤ suma transferencias: "+this.sumaTotal);
      }
      this.rdy = true;
    });
  }

  //abrir modal
  openM(id: any) {
    this.modalDelete = true;
  }
  //close
  closeM() {
    this.modalDelete = false;
  }

  //borrar transferencia
  eliminarT(id: any) {
    this._transfService.deleteTransf(id).subscribe(
      res => {
        console.log(res);
      },
      error => {
        console.log(error);
      }
    );
  }

  editar(id: any){
    this.idTransf.emit(id);
    this.estadoT.emit(true);
    this.editaT.emit(false);
  }
}
