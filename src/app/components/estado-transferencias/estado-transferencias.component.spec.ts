import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstadoTransferenciasComponent } from './estado-transferencias.component';

describe('EstadoTransferenciasComponent', () => {
  let component: EstadoTransferenciasComponent;
  let fixture: ComponentFixture<EstadoTransferenciasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EstadoTransferenciasComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstadoTransferenciasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
