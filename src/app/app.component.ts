import { Component, OnInit } from '@angular/core';
import { ConnectService } from './services/connect.service';
import { JwtHelperService } from '@auth0/angular-jwt';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'app';
  estado: boolean;
  invTope: number;
  //datosToken: Array<any>;
  //stringToken: string;
  resultJwt: Array<any>;
  getUrl: string;
  nombreObjetivo: string;

  codCom: number;
  numPol: string;
  codRam: number;
  numSpt: number;

  constructor(public _transfService: ConnectService) {}

  ngOnInit(): void {
    //this.invTope = 12000;
    this.estado = true;
    //tomar url
    this.getUrl = window.location.href
    this.getUrl = this.getUrl.replace(/.*\/\/[^\/]*/, '');
    this.getUrl = this.getUrl.replace(/\//g, '');
    this.guardarDatosJWT(this.getUrl)

    //buscar token
    // this._transfService.tokenInicial().subscribe(
    //   (res: any) => {
    //     let datos = res;
    //     this.datosToken = datos.Data.tokenAutorizacion;
    //     //envimos token
    //     console.log("esto es: "+ this.datosToken);
    //     this.obtenerLink(this.datosToken);
    //   },
    //   error => {
    //     console.log(error);
    //   }
    // );
  }

  estadoTransferencia(): void {
    this.estado = true;
  }

  agregarTransferencia(): void {
    this._transfService.editar = false;
    this.estado = false;
  }

  guardarDatosJWT(v){
    const helper = new JwtHelperService();
    const decodedToken = helper.decodeToken(v);
    this.resultJwt = decodedToken;
    this._transfService.codRamo = decodedToken.codRamo;
    this.codRam = decodedToken.codRamo;
    this._transfService.codigoCompania = decodedToken.codigoCompania;
    this.codCom = decodedToken.codigoCompania;
    this._transfService.fecha = decodedToken.fecha;
    this._transfService.numPoliza = decodedToken.numPoliza;
    this.numPol = decodedToken.numPoliza;
    this._transfService.numSpto = decodedToken.numSpto;
    this.numSpt = decodedToken.numSpto;
    this._transfService.numeroDocumento = decodedToken.numeroDocumento;
    this._transfService.tipoDocumento = decodedToken.tipoDocumento;
    console.log(this.resultJwt);
    this.runObj();
  }

  //mandamos token para obtener link y guardar los datos en nuestro servicio
  // obtenerLink(token: any) {
  //   this._transfService.getJwt(token).subscribe(
  //     (res: any) => {
  //       console.log(res.Data.url);
  //       this.stringToken = res.Data.url;
  //       this.stringToken = this.stringToken.replace(/.*\/\/[^\/]*/, '');
  //       this.stringToken = this.stringToken.replace(/\//g, '');
  //       const helper = new JwtHelperService();
  //       const decodedToken = helper.decodeToken(this.stringToken);
  //       this.resultJwt = decodedToken;
  //       this._transfService.codRamo = decodedToken.codRamo;
  //       this._transfService.codigoCompania = decodedToken.codigoCompania;
  //       this._transfService.fecha = decodedToken.fecha;
  //       this._transfService.numPoliza = decodedToken.numPoliza;
  //       this._transfService.numSpto = decodedToken.numSpto;
  //       this._transfService.numeroDocumento = decodedToken.numeroDocumento;
  //       this._transfService.tipoDocumento = decodedToken.tipoDocumento;
  //       console.log(this._transfService.codRamo);
  //       this.runObj();
  //     },
  //     error => {
  //       console.log(error);
  //     }
  //   );
  // }

  runObj() {
    this._transfService.getObjetivo().subscribe(
      (res: any) => {
        this._transfService.tipoMoneda = res.Data.moneda
        console.log('¤ objetivo: ¬');
        console.log(res);
        this.invTope = res.Data.importeInversion;
        this.nombreObjetivo = res.Data.nombreObjetivo;
        this._transfService.nombreObjetivo = res.Data.nombreObjetivo;
        this._transfService.importeInversion = res.Data.importeInversion
        console.log("¤ monto inversion ¬: "+this.invTope);
      },
      error => {
        console.log(error);
      }
    );
  }
}
