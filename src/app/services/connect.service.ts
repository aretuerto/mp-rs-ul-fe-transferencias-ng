import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DatosTransf } from '../models/transferencia';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ConnectService {
  selectedTransf: DatosTransf;
  transferencias: DatosTransf[];
  codigoCompania: number;
  tipoDocumento: string;
  numeroDocumento: string;
  numPoliza: string;
  codRamo: number;
  numSpto: number;
  fecha: string;
  montoTotal: number;
  tipoMoneda: string;
  nombreObjetivo: string;
  importeInversion: string;
  editar: boolean;
  idEdicion: number;

  //url = 'http://0662-38-25-12-79.ngrok.io';
  url = environment.API_URL

  constructor(private _http: HttpClient) {
    this.selectedTransf = new DatosTransf();
  }
  //agregar transferencia
  postTransf(datosTransf: DatosTransf) {
    const httpOption: Object = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Accept: '*/*'
      })
    };
    return this._http.post(this.url + '/transferencia', datosTransf, httpOption);
  }

  //obtener transferencias (sin uso)
  // getTransf() {
  //   return this._http.get<DatosTransf[]>(this.URL_API);
  // }

  //obtener transferencias
  listTransferencias(a: any,b: any,c: any,d: any) {
    return this._http.post(this.url + '/transferencias/listar', {
      codigoCompania: a,
      numPoliza: b,
      codigoRamo: c,
      numSpto: d
    });
  }

  //obtener transferencia para editar
  editarTransferencia(id: any){
    return this._http.post(this.url+'/transferencias/listar?id='+id, {
      codigoCompania: this.codigoCompania,
      numPoliza: this.numPoliza,
      codigoRamo: this.codRamo,
      numSpto: this.numSpto
    })
  }

  //obtener lista de bancos
  getBancos() {
    return this._http.get<any[]>(this.url + '/transferencia/bancos');
  }

  //modificar transferencia
  putTransf(datosTransf: any) {
    return this._http.put(this.url + '/transferencia/bancos', datosTransf);
  }

  //borrar transferencia
  deleteTransf(id: any) {
    return this._http.delete(this.url + '/transferencias/listar/&id=' + id);
  }

  //objetivo
  getObjetivo() {
    return this._http.get<any[]>(
      this.url +
        '/transferencia/objetivo?codCompania=' +
        this.codigoCompania +
        '&codRamo=' +
        this.codRamo +
        '&numPoliza=' +
        this.numPoliza +
        '&numSpto=' +
        this.numSpto +
        '&tipoDocumento=' +
        this.tipoDocumento +
        '&numeroDocumento=' +
        this.numeroDocumento
    );
  }
}
